<?php

include 'connection.php';

include '../entidades/usuarios.php';

class usuarioDao extends connection
{
    protected static $cnx;

    private static function getConexion(){
        self::$cnx =  connection::connect();
    }

    private static function desconctar(){
        self::$cnx = null; 
    }

    // metodo que sirve para validar el login
    public static function login($user)
    {
        $query = "SELECT * FROM  usuarios WHERE user = :user AND password = :password";

        self::getConexion();

        $resultado = self::$cnx->prepare($query);
        
        $resultado->bindParam(":user", $user->getUser());
        $resultado->bindParam(":password", $user->getPassword());

        $resultado->execute();

        if($resultado->rowCount() > 0 ){
            $filas = $resultado->fetch();
            if($filas["user"]==$user->getUser() 
            && $filas ["password"]==$user->getPassword()){
                return true;
            }
        }
           return false; 
        
    }
}